#!/bin/sh
# Script to set up one of the nodes as a NAT gateway for all other nodes.
# This is used to ensure that all nodes in the cluster can still originate
# traffic to the external network even if there are no public addresses
# available.
#

. $CTDB_BASE/functions
loadconfig

[ -z "$CTDB_NATGW_NODES" ] && exit 0

# Update capabilities to show whether we support teh NATGW capability or not
if [ "$CTDB_NATGW_SLAVE_ONLY" = "yes" ] ; then
	ctdb setnatgwstate off
else
	ctdb setnatgwstate on
fi

delete_all() {
	local _ip=`echo $CTDB_NATGW_PUBLIC_IP | cut -d '/' -f1`
	local _maskbits=`echo $CTDB_NATGW_PUBLIC_IP | cut -d '/' -f2`

	[ -z "$CTDB_NATGW_PUBLIC_IFACE" ] || {
	    delete_ip_from_iface $CTDB_NATGW_PUBLIC_IFACE $_ip $_maskbits 2>/dev/null
	}
	ip route del 0.0.0.0/0 metric 10 >/dev/null 2>/dev/null

	# Delete the masquerading setup from a previous iteration where we
	# were the NAT-GW
	iptables -D POSTROUTING -t nat -s $CTDB_NATGW_PRIVATE_NETWORK -d ! $CTDB_NATGW_PRIVATE_NETWORK -j MASQUERADE >/dev/null 2>/dev/null

	# remove any iptables rule we may have on this address
	iptables -D INPUT -p tcp --syn -d $_ip/32 -j REJECT 2>/dev/null
}

case "$1" in 
    startup)
	[ -z "$CTDB_PUBLIC_ADDRESSES" ] && {
		CTDB_PUBLIC_ADDRESSES=/etc/ctdb/public_addresses
	}
	egrep "^$CTDB_NATGW_PUBLIC_IP[ \t]" $CTDB_PUBLIC_ADDRESSES >/dev/null
	[ "$?" = "0" ] && {
		echo ERROR: NATGW configured to use a public address. NATGW must not use a public address.
		exit 1
	}

	# do not send out arp requests from loopback addresses
	echo 2 > /proc/sys/net/ipv4/conf/all/arp_announce
	;;

    recovered|updatenatgw|ipreallocated)
	MYPNN=`ctdb pnn | cut -d: -f2`
	NATGWMASTER=`ctdb natgwlist | head -1 | sed -e "s/ .*//"`
	NATGWIP=`ctdb natgwlist | head -1 | sed -e "s/^[^ ]* *//"`

	CTDB_NATGW_PUBLIC_IP_HOST=`echo $CTDB_NATGW_PUBLIC_IP | sed -e "s/\/.*/\/32/"`

	# block all incoming connections to the natgw address
	iptables -D INPUT -p tcp --syn -d $CTDB_NATGW_PUBLIC_IP_HOST -j REJECT 2>/dev/null 
	iptables -I INPUT -p tcp --syn -d $CTDB_NATGW_PUBLIC_IP_HOST -j REJECT 2>/dev/null 


	if [ "$NATGWMASTER" = "-1" ]; then
		echo "There is no NATGW master node"
		exit 1
	fi

	delete_all

	if [ "$MYPNN" = "$NATGWMASTER" ]; then
		# This is the first node, set it up as the NAT GW
		echo 1 >/proc/sys/net/ipv4/ip_forward
		iptables -A POSTROUTING -t nat -s $CTDB_NATGW_PRIVATE_NETWORK -d ! $CTDB_NATGW_PRIVATE_NETWORK -j MASQUERADE

		# block all incoming connections to the natgw address
		CTDB_NATGW_PUBLIC_IP_HOST=`echo $CTDB_NATGW_PUBLIC_IP | sed -e "s/\/.*/\/32/"`
		iptables -D INPUT -p tcp --syn -d $CTDB_NATGW_PUBLIC_IP_HOST -j REJECT 2>/dev/null
		iptables -I INPUT -p tcp --syn -d $CTDB_NATGW_PUBLIC_IP_HOST -j REJECT 2>/dev/null

		ip addr add $CTDB_NATGW_PUBLIC_IP dev $CTDB_NATGW_PUBLIC_IFACE
		ip route add 0.0.0.0/0 via $CTDB_NATGW_DEFAULT_GATEWAY >/dev/null 2>/dev/null
	else
		# This is not the NAT-GW
		# Assign the public ip to the private interface and make
		# sure we dont respond to ARPs.
		# We do this so that the ip address will exist on a
		# non-loopback interface so that samba may send it along in the
		# KDC requests.
		ip route add 0.0.0.0/0 via $NATGWIP metric 10
		# Make sure winbindd does not stay bound to this address
		# if we are no longer natgwmaster
		smbcontrol winbindd ip-dropped $CTDB_NATGW_PUBLIC_IP >/dev/null 2>/dev/null
	fi

	# flush our route cache
	echo 1 > /proc/sys/net/ipv4/route/flush
	;;

    shutdown|stopped|removenatgw)
	delete_all
	;;

    *)
	ctdb_standard_event_handler "@"
	;;
esac

exit 0
